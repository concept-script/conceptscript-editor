export interface Position {
	line: number
	column: number
}

export interface Selection {
	start: Position
	end: Position
}

export interface ContentEditableState {
	content: string
	selections: Selection[]
}

export enum ChangeType {
	NON_BREAKING_CHARACTER_TYPED, // example: typed A or B or C...
	BREAKING_CHARACTER_TYPED,     // space or enter key
	TEXT_PASTED,                  // example: Ctrl+V or Cmd+V
	TEXT_DELETED_BACKWARD,        // backspace key
	TEXT_DELETED_FORWARD,         // delete key
	CARET_POSITION_UPDATED,       // mouse click or arrow keys
	UNDO_REDO,                    // example: Ctrl+Z or Ctrl+Y
}

/**
 * Similar to console.log() but returns the first parameter.
 * Useful for probing the code.
 */
export function debug<T>(data: T, ...additionalData: any[]): T {
	console.log(...[data, ...additionalData])
	return data
}

export function isCaretInNode(caret: Range, node: Node): boolean {
	const nodeRange = new Range()
	nodeRange.selectNodeContents(node)

	return caret.compareBoundaryPoints(Range.START_TO_START, nodeRange) >= 0
		&& caret.compareBoundaryPoints(Range.END_TO_START, nodeRange) <= 0
}

export function getAllTextNodes(parent: Node): Node[] {
	const nodes = []
	let walker = document.createTreeWalker(parent, NodeFilter.SHOW_TEXT, null)
	let node: Node
	while (node = walker.nextNode()) nodes.push(node)
	return nodes
}
