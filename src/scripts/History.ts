import { StateStack } from './StateStack'
import { ChangeType, ContentEditableState } from './utils'

export class History {
	private past: StateStack
	private future: StateStack
	private listener: (e: KeyboardEvent) => void
	private lastChangeType: ChangeType
	private caretChangeToBePushed: ContentEditableState = null

	public constructor(currentState: ContentEditableState, maxSize = Infinity) {
		this.past   = new StateStack(maxSize)
		this.future = new StateStack()
		this.past.push(currentState)
	}

	public saveChange(newState: ContentEditableState, changeType: ChangeType) {
		if (changeType === ChangeType.CARET_POSITION_UPDATED) {
			// updating caret position modify the history stacks only if followed by another change
			this.caretChangeToBePushed = newState
		} else {
			// take into account the last caret update
			if (this.caretChangeToBePushed !== null) {
				this.past.pop()
				this.past.push(this.caretChangeToBePushed)
				this.caretChangeToBePushed = null
			}

			// concatenate some changes by popping the last state before pushing the new one
			if (this.lastChangeType === ChangeType.BREAKING_CHARACTER_TYPED && changeType === ChangeType.NON_BREAKING_CHARACTER_TYPED
				|| this.lastChangeType === changeType && (
					changeType === ChangeType.NON_BREAKING_CHARACTER_TYPED
					|| changeType === ChangeType.TEXT_DELETED_BACKWARD
					|| changeType === ChangeType.TEXT_DELETED_FORWARD
				)
			) {
				this.past.pop()
			}

			this.past.push(newState)
			this.future.clear()
		}

		this.lastChangeType = changeType
	}

	public mount(setState: (s: ContentEditableState) => void) {
		if (this.listener) throw 'history already mounted'

		document.addEventListener('keydown', this.listener = (e: KeyboardEvent) => {
			if (!e.ctrlKey || e.metaKey || e.altKey) return

			const key = e.key.toUpperCase()
			let newState: ContentEditableState = null

			if (!e.shiftKey && key === 'Z') {
				newState = this.undo()
			} else if (e.shiftKey && key === 'Z' || !e.shiftKey && key === 'Y') {
				newState = this.redo()
			} else {
				return
			}

			// at this point a undo or a redo as been attempted

			// prevent the browser default behavior
			e.preventDefault()

			// update the state if the undo or redo is successful
			if (newState !== null) {
				setState(newState)
				this.lastChangeType = ChangeType.UNDO_REDO
			}
		})
	}

	public unmount() {
		if (!this.listener) throw 'history not mounted'
		document.removeEventListener('keydown', this.listener)
	}

	/**
	 * @return the new state or null if undo isn't possible
	 */
	private undo(): ContentEditableState {
		if (this.past.size <= 1) return null

		this.future.push(this.past.pop())
		return this.past.top()
	}

	/**
	 * @return the new state or null if redo isn't possible
	 */
	private redo(): ContentEditableState {
		if (this.future.isEmpty()) return null

		const state = this.future.pop()
		this.past.push(state)
		return state
	}
}
