import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import '../styles/index.scss'

const example = 'print("Hello World!")'
ReactDOM.render(<App content={example} />, document.getElementById('app'))
