import React, { ClipboardEvent, Component, createRef, KeyboardEvent } from 'react'
import { encodeHTML } from 'entities'
import { History } from './History'
import { ChangeType, ContentEditableState, getAllTextNodes, isCaretInNode, Position } from './utils'

interface Props {
	content: string
}

export default class App extends Component<Props, ContentEditableState> {
	private contentEditable = createRef<HTMLDivElement>()
	private history: History
	private pasteFlag = false // set to true whenever user paste something, reset when onModify is fired
	private lastKeyPressed: string

	public constructor(props: Props) {
		super(props)

		this.state = {
			content: props.content,
			selections: [
				{
					start: {
						line: 1,
						column: 1,
					},
					end: {
						line: 1,
						column: 1,
					},
				},
			],
		}

		this.history = new History(this.state)
	}

	public componentDidMount() {
		this.history.mount(s => this.setState(s))
	}

	public componentWillUnmount() {
		this.history.unmount()
	}

	public render() {
		return <div
			contentEditable={true}
			dangerouslySetInnerHTML={{__html: this.escapedHtml}}
			ref={this.contentEditable}
			onPaste={e => this.onPaste(e)}
			onInput={() => this.onInput()}
			onKeyDown={e => this.onKeyDown(e)}
			onMouseDown={() => {}}
			onMouseUp={() => {}}
			onDragStart={e => e.preventDefault()} // disable text dragging because it's not implemented (it would not work)
		/>
	}

	public componentDidUpdate() {
		this.setSelections()
	}

	public shouldComponentUpdate(nextProps: Readonly<Props>, nextState: Readonly<ContentEditableState>): boolean {
		return this.state.content !== nextState.content
	}

	private onPaste(event: ClipboardEvent<HTMLDivElement>) {
		this.pasteFlag = true
		event.preventDefault()

		const text = event.clipboardData.getData('text/plain')
			.replace(/ /g, '\u00a0')
			.replace(/\r/g, '')
			.split('\n')
			.map(encodeHTML)
			.join('<br>')

		document.execCommand('insertHTML', false, text)
	}

	private onInput() {
		let changeType: ChangeType

		if (this.pasteFlag) {
			this.pasteFlag = false
			changeType = ChangeType.TEXT_PASTED
		} else if (this.lastKeyPressed === 'Backspace') {
			changeType = ChangeType.TEXT_DELETED_BACKWARD
		} else if (this.lastKeyPressed === 'Delete') {
			changeType = ChangeType.TEXT_DELETED_BACKWARD
		} else if (this.lastKeyPressed === ' ' || this.lastKeyPressed === 'Enter') {
			changeType = ChangeType.BREAKING_CHARACTER_TYPED
		} else {
			changeType = ChangeType.NON_BREAKING_CHARACTER_TYPED
		}

		const newState = this.getNewState()
		console.log(ChangeType[changeType])
		this.history.saveChange(newState, changeType)
		this.setState(newState)
	}

	private onKeyDown(event: KeyboardEvent<HTMLDivElement>) {
		this.lastKeyPressed = event.key
	}

	private getNewState(): ContentEditableState {
		const caret = window.getSelection().getRangeAt(0)
		const caretPos: Position = {
			line: 1,
			column: 1,
		}
		let caretFound = false
		let content = ''

		dfs(this.contentEditable.current)
		content = content.replace(/\u00a0/g, ' ')
		return {
			content,
			selections: [
				{
					start: caretPos,
					end: caretPos,
				},
			],
		}

		function newLine() {
			if (!caretFound) {
				++caretPos.line
				caretPos.column = 1
			}
			content += '\n'
		}

		function dfs(element: Node) {
			let firstDiv = true
			const childNodes = element.childNodes

			for (let i = 0 ; i < childNodes.length; ++i) {
				const node = childNodes[i]
				const caretInNode = !caretFound && isCaretInNode(caret, node)

				switch (node.nodeName) {
					case '#text':
						if (!caretFound) {
							if (caretInNode) {
								const preCaretRange = document.createRange()
								preCaretRange.selectNodeContents(node)
								preCaretRange.setEnd(caret.endContainer, caret.endOffset)
								caretPos.column += preCaretRange.toString().length - caret.toString().length
							} else {
								caretPos.column += node.textContent.length
							}
						}
						content += node.textContent
						break

					case 'BR':
						if (i < childNodes.length - 1) newLine()
						break

					case 'P':
					case 'DIV':
						if (firstDiv) firstDiv = false
						else newLine()
						dfs(node)
						break

					default:
						dfs(node)
				}

				if (caretInNode) caretFound = true
			}
		}
	}

	private setSelections() {
		interface CaretDestination {
			node: Node
			offset: number
		}

		const pos = this.state.selections[0].start
		const line = this.contentEditable.current.childNodes[pos.line - 1]
		const destination = getCaretDestination(line)
		setCaretPosition(destination)

		function getCaretDestination(destinationLine: Node): CaretDestination {
			const nodes = getAllTextNodes(destinationLine)

			// destination
			let node: Node
			let offset = pos.column - 1

			// dichotomic search to find the text node where the caret is
			for (let i = 0; i < nodes.length; ++i) {
				if (offset > nodes[i].nodeValue.length && nodes[i + 1]) {
					// remove amount from the position, go to next node
					offset -= nodes[i].nodeValue.length
				} else {
					node = nodes[i]
					break
				}
			}

			// if there is no text node on this line, the line is empty and the caret is in the parent div
			if (nodes.length === 0) {
				node = line
			}

			return {
				node,
				offset,
			}
		}

		function setCaretPosition({ node, offset }: CaretDestination) {
			const range = document.createRange()
			range.setStart(node, offset)
			range.collapse(true)

			const selection = window.getSelection()
			selection.removeAllRanges()
			selection.addRange(range)
		}
	}

	private get escapedHtml(): string {
		return this.state.content
			.replace(/ /g, '\u00a0')
			.split('\n')
			.map(encodeHTML)
			.map(line => `<div>${line || '<br>'}</div>`)
			.join('')
	}
}
