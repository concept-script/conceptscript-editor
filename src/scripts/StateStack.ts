import { ContentEditableState } from './utils'

export class StateStack {
	private readonly maxSize: number
	private _size: number

	// We don't use an array of states because it would be heavy on memory:
	// An array of number and an array string are lighter than an array of objects.
	private stringStorage: string[]
	private numberStorage: number[]
	private meta: number[]

	public constructor(maxSize = Infinity) {
		this.maxSize = maxSize
		this.clear()
	}

	public push(state: ContentEditableState) {
		this.stringStorage.push(state.content)
		this.meta.push(state.selections.length)
		for (const selection of state.selections) {
			this.numberStorage.push(selection.end.column)
			this.numberStorage.push(selection.end.line)
			this.numberStorage.push(selection.start.column)
			this.numberStorage.push(selection.start.line)
		}

		if (this._size === this.maxSize) {
			this.stringStorage.splice(0, 1)
			this.numberStorage.splice(0, this.meta[0] * 4 + 1)
			this.meta.splice(0, 1)
		} else {
			++this._size
		}
	}

	public pop(): ContentEditableState {
		if (this.isEmpty()) throw 'can\'t pop an empty stack'

		--this._size

		const state: ContentEditableState = {
			content: this.stringStorage.pop(),
			selections: [],
		}

		const nbOfSelections = this.meta.pop()
		for (let i = 0; i < nbOfSelections; ++i) {
			state.selections.unshift({
				start: {
					line: this.numberStorage.pop(),
					column: this.numberStorage.pop(),
				},
				end: {
					line: this.numberStorage.pop(),
					column: this.numberStorage.pop(),
				},
			})
		}

		return state
	}

	public top(): ContentEditableState {
		if (this.isEmpty()) throw 'can\'t get last element of an empty stack'

		const state = this.pop()
		this.push(state)

		return state
	}


	public clear() {
		if (this._size === 0) return
		this._size = 0
		this.stringStorage = []
		this.numberStorage = []
		this.meta = []
	}

	public isEmpty(): boolean {
		return this._size === 0
	}

	public get size(): number {
		return this._size
	}
}
